<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [
    'uses' => 'HomeController@start'
]);

Route::get('/forbidden', function (){
    return view('errors.403');
});

Route::get('404', [
    'uses' => 'HomeController@error404','as'=>'404',
]);

Route::get('405', [
    'uses' => 'HomeController@error405','as'=>'405',
]);

//post sign in
Route::any('signIn', [
    'uses' => 'AuthController@signIn','as'=>'signIn',
]);

Auth::routes();

Route::group(['middleware'=>'auth'], function () {

  //  Route::get('/home', 'HomeController@index')->name('home');

    Route::get('home', [
        'middleware' => ['auth', 'roles'], // A 'roles' middleware must be specified
        'uses' => 'HomeController@index','as'=>'home',
        'roles' => ['Presenter', 'Admin','SuperAdmin'] // Only an presenters,admin, or a super-admin can access this route
    ]);

    Route::get('inbox', [
        'middleware' => ['auth', 'roles'], // A 'roles' middleware must be specified
        'uses' => 'InboxController@index','as'=>'inbox',
        'roles' => ['Presenter', 'Admin','SuperAdmin'] // Only an presenters,admin, or a super-admin can access this route
    ]);

    Route::get('outbox', [
        'middleware' => ['auth', 'roles'], // A 'roles' middleware must be specified
        'uses' => 'InboxController@outbox','as'=>'outbox',
        'roles' => ['Presenter', 'Admin','SuperAdmin'] // Only an presenters,admin, or a super-admin can access this route
    ]);


    Route::get('shows', [
        'middleware' => ['auth', 'roles'], // A 'roles' middleware must be specified
        'uses' => 'HomeController@index',
        'roles' => ['Admin', 'SuperAdmin'] // Only an admin, or a super-admin can access this route
    ]);

    Route::get('users', [
        'middleware' => ['auth', 'roles'], // A 'roles' middleware must be specified
        'uses' => 'HomeController@index',
        'roles' => ['SuperAdmin'] // Only a super-admin can access this route
    ]);


    Route::get('ticker', [
        'middleware' => ['auth', 'roles'], // A 'roles' middleware must be specified
        'uses' => 'TestController@showTicker',
        'roles' => ['Presenter', 'Admin','SuperAdmin'] // Only an presenters,admin, or a super-admin can access this route
    ]);
});

