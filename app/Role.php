<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
//    protected $table = 'roles';
//
//    public function users()
//    {
//        return $this->hasMany('App\User', 'roles_id', 'id');
//    }

    public function users(){
        return $this->belongsToMany('App\Role');
    }

    public function scopeRole($query,$r){
        return $query->where('name',$r);
    }
}
