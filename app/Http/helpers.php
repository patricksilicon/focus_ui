<?php

function checkRole($permissions){
    $userAccess = getMyPermission(auth()->user()->roles_id);
    foreach ($permissions as $key => $value) {
        if($value == $userAccess){
            return true;
        }
    }
    return false;
}

function getMyPermission($id)
{
    switch ($id) {
        case 1:
            return 'Presenter';
            break;
        case 2:
            return 'Admin';
            break;
        default:
            return 'SuperAdmin';
            break;
    }
}

?>
