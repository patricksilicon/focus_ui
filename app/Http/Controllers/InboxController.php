<?php

namespace App\Http\Controllers;

use App\Inbox;
use App\Outbox;
use Illuminate\Http\Request;

class InboxController extends Controller
{
    public function index(){

        $inbox=Inbox::where('short_code','22644')
            ->orderBy('id','desc')
            ->get();

        return view('message.inbox', compact('inbox'));
    }

    public function outbox(){

        $outbox=Outbox::where('sender','22644')
            ->orderBy('id','desc')
            ->get();

        return view('message.outbox', compact('outbox'));
    }
}
