<?php

namespace App\Http\Controllers;

use App\Ticker;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function showTicker(){

        $ticker=Ticker::first();

        return view('test.ticker',compact('ticker'));
    }
}
