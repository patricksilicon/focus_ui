<?php

namespace App\Http\Controllers;



use App\Inbox;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    //show error page 404 not found
    public function error404()
    {
        return view('errors.404');

    }

    //show error page 405 method not allowed
     public function error405(){
        return view('errors.405');
    }

    //redirect to login page on start
    public function start(){
        return redirect(route('login'));
    }



}
