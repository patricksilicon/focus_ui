<?php

namespace App\Http\Controllers;

use App\User;
use http\Exception;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    public function signIn(Request $request)
    {
        try{


            $data = $request->all();


            $checkUser = $data['email'];

            $inactive = 0;

            //$activated = User::where('id','=',$userCheck)->first()['active'];

            $loginUser = User::where('email',$checkUser)->first();

            if(empty($loginUser)){
                return redirect()->back()->with('error1', 'User does not exist');

            }



            if($loginUser->active != 1)
            {

                return redirect()->back()->with('error1', 'Your account is pending an approval or it is deactivated. Contact your account manager.');

            }



            if($loginUser !== $inactive)
            {


                $userdata = array(
                    'email' => $request->get('email'),
                    'password' => $request->get('password')
                );


                if(Auth::attempt($userdata))
                {


                        return redirect()->route('inbox');


                }

                return redirect()->back()->with('error1', 'Invalid credentials');
            }


        } catch ( Exception $e) {
            return redirect()->back()->with('error1','Some thing went wrong!');

        }

    }
}
