<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function index(){

      $role= Auth::user()->roles_id;

      $users=User::where('roles_id','<',$role)->get();

      return view('user.users',compact('users'));
    }
}
