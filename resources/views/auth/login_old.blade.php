<!DOCTYPE html>
<html lang="en">

<head>
    <title>Focus Tv</title>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap-responsive.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/css/maruti-login.css')}}"/>
</head>
<body>
<div id="loginbox">
    <form id="loginform" class="form-vertical" method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}
        <div class="control-group normal_text"><h3><img src="{{asset('assets/img/logo.png')}}" alt="Logo"/></h3></div>

        {{--<div class="control-group {{ $errors->has('email') ? ' has-error' : '' }} ">
            <div class="controls ">
                <div class="main_input_box">
                    <span class="add-on"><i class="icon-user"></i></span><input class="form-group"  name="email" type="text" placeholder="Username" value="{{ old('email') }}" required autofocus />
                    @if ($errors->has('email'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

        </div>--}}

        <div class="control-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <div class="controls">
            <div class="main_input_box">
                <span class="add-on"><i class="icon-user"></i></span>
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required
                       autofocus>

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            </div>
        </div>

        <div class="control-group{{ $errors->has('password') ? ' has-error' : '' }}">

            <div class="controls">
                <div class="main_input_box">
                    <span class="add-on"><i class="icon-lock"></i></span>
                    <input id="password" type="password" class="form-control" name="password" required>

                @if ($errors->has('password'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                @endif
            </div>
            </div>
        </div>

        {{--<div class="control-group">--}}
            {{--<div class="controls">--}}
                {{--<div class="main_input_box">--}}
                    {{--<span class="add-on"><i class="icon-lock"></i></span><input type="password" placeholder="Password"/>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        <div class="form-actions">
            <span class="pull-left"><a href="#" class="flip-link btn btn-inverse"
                                       id="to-recover">Lost password?</a></span>
            <span class="pull-right"><input type="submit" class="btn btn-success" value="Login"/></span>
        </div>
    </form>
    <form id="recoverform" action="#" class="form-vertical">
        <p class="normal_text">Enter your e-mail address below and we will send you instructions how to recover a
            password.</p>

        <div class="controls">
            <div class="main_input_box">
                <span class="add-on"><i class="icon-envelope"></i></span><input type="text"
                                                                                placeholder="E-mail address"/>
            </div>
        </div>

        <div class="form-actions">
            <span class="pull-left"><a href="#" class="flip-link btn btn-inverse"
                                       id="to-login">&laquo; Back to login</a></span>
            <span class="pull-right"><input type="submit" class="btn btn-info" value="Recover"/></span>
        </div>
    </form>
</div>

<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/maruti.login.js')}}"></script>
<script src="{{ asset('js/app.js') }}"></script>
</body>

</html>
