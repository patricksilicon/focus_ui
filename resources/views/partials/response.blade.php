@if(Session::has('success1'))
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
            <i class="fa fa-window-close" aria-hidden="true"></i>
        </button>
        {{ Session::get('success1') }}
    </div>
@elseif(Session::has('error1'))
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
            <i class="fa fa-window-close" aria-hidden="true"></i>
        </button>
        {{ Session::get('error1') }}
    </div>


@elseif(Session::has('status'))
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
            <i class="fa fa-window-close" aria-hidden="true"></i>
        </button>
        {{ Session::get('status') }}
    </div>

    <!--Alerts on top -->
@elseif(Session::has('error'))
    <div id="alerttopright" class="myadmin-alert myadmin-alert-img alert-danger myadmin-alert-top-right" style="display: block;">
        <img src="{{asset('images/users/genu.jpg')}}" class="img" alt="img"><a href="#" class="closed">×</a>
        <h4>Sorry</h4> {{ Session::get('error') }}</div>


@elseif(Session::has('success'))
    <div id="alerttopright" class="myadmin-alert myadmin-alert-img alert-success myadmin-alert-top-right" style="display: block;">
        <img src="{{asset('images/users/genu.jpg')}}" class="img" alt="img"><a href="#" class="closed">×</a>
        <h4>Success</h4> {{ Session::get('success') }}</div>
@endif

<!-- 

@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
            </div>
@endif -->
<script type="text/javascript">
    //Alerts
    $(".myadmin-alert .closed").click(function(event) {
        $(this).parents(".myadmin-alert").fadeToggle(350);
        return false;
    });
</script>