@extends('layouts.demoNav')
@section('content')
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body" style="text-align: center">
                            <h1 style="color: black; font-size: 85px;"> 404 </h1>
                            <h2>Page Not Found<br>
                                <h3><br>
                                    You Tried to access page that doesn't exist.<br>
                                    <br>
                                </h3>
                            </h2>
                        </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
