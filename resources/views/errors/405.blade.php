@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="row">
                <div class="row" style="text-align: center">
                    <h1 style="color: red; font-size: 85px;"> 405 </h1>
                    <h2>Method No Allowed<br>
                        {{--<h3> <br>--}}
                            {{--You Tried to access page that doesn't exist.<br>--}}
                            {{--<br>--}}
                        {{--</h3>--}}
                    </h2>
                </div>
            </div>
        </div>
    </div>
@endsection
